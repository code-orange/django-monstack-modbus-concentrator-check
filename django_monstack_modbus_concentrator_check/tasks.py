from celery import shared_task
from pymodbus.client.sync import ModbusTcpClient

from django_monstack_modbus_concentrator_models.django_monstack_modbus_concentrator_models.models import (
    ModbusDevice,
)


@shared_task(name="check_modbus")
def check_modbus():
    modbus_device_checks = ModbusDevice.objects.all()

    for modbus_device_check in modbus_device_checks:
        intro_txt = "MODBUS " + str(modbus_device_check.id) + ": "

        modbus_client = ModbusTcpClient(
            modbus_device_check.address, modbus_device_check.port
        )
        result = modbus_client.read_coils(int(modbus_device_check.read_coil))

        if result.bits[0] == int(modbus_device_check.good_state):
            print(intro_txt + "INPUT GOOD")
            if modbus_device_check.write_coil_value == "1":
                revert_value = 0
                print(
                    intro_txt
                    + "INPUT GOOD - revert write coil - value: "
                    + str(revert_value)
                )
                modbus_client.write_coil(
                    int(modbus_device_check.write_coil), revert_value
                )
            else:
                revert_value = 1
                print(
                    intro_txt
                    + "INPUT GOOD - revert write coil - value: "
                    + str(revert_value)
                )
                modbus_client.write_coil(
                    int(modbus_device_check.write_coil), revert_value
                )
        else:
            # input in failed state
            print(intro_txt + "INPUT BAD")
            if modbus_device_check.write_coil:
                modbus_client.write_coil(
                    int(modbus_device_check.write_coil),
                    int(modbus_device_check.write_coil_value),
                )

    return
