from django.core.management.base import BaseCommand

from django_monstack_modbus_concentrator_check.django_monstack_modbus_concentrator_check.tasks import (
    check_modbus,
)


class Command(BaseCommand):
    help = "check_modbus"

    def handle(self, *args, **options):
        # New Installation Date
        check_modbus()
